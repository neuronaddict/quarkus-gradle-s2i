# quarkus-gradle-s2i
FROM registry.access.redhat.com/ubi9/openjdk-17

LABEL maintainer="neuronaddict"

ENV BUILDER_VERSION 1.0

LABEL io.k8s.description="Platform for building quarkus with gradle" \
      io.k8s.display-name="quarkus gradle builder" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,gradle,openjdk,quarkus" \
      io.openshift.s2i.scripts-url="image:///usr/local/s2i"

USER 0

COPY ./s2i/bin/ /usr/local/s2i

ENV GRADLE_USER_HOME /opt/gradle_home/

RUN mkdir -p $GRADLE_USER_HOME /opt/app-root \
    && chown -R 1001:0 $GRADLE_USER_HOME /opt/app-root \
    && chmod -R g=u $GRADLE_USER_HOME /opt/app-root \
    && chmod +x /usr/local/s2i/*

WORKDIR /opt/app-root

USER 1001

EXPOSE 8080

CMD ["/usr/local/s2i/usage"]
