IMAGE_NAME = s2i-gradle
DOCKER_BINARY = $(shell /bin/bash -c 'which podman docker | head -1' )

.PHONY: build
build:
	$(DOCKER_BINARY) build -t $(IMAGE_NAME) .

.PHONY: test
test:
	$(DOCKER_BINARY) build -t $(IMAGE_NAME)-candidate .
	IMAGE_NAME=$(IMAGE_NAME)-candidate test/run
